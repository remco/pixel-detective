use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};
use std::path::Path;
use std::fs::File;
use std::io::Read;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/remcokranenburg/PixelDetective/window.ui")]
    pub struct PixelDetectiveWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub canvas: TemplateChild<gtk::DrawingArea>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PixelDetectiveWindow {
        const NAME: &'static str = "PixelDetectiveWindow";
        type Type = super::PixelDetectiveWindow;
        type ParentType = gtk::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            klass.install_action("win.open-file", None, move |win, _, _| {
                win.open_file();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PixelDetectiveWindow {}
    impl WidgetImpl for PixelDetectiveWindow {}
    impl WindowImpl for PixelDetectiveWindow {}
    impl ApplicationWindowImpl for PixelDetectiveWindow {}
}

glib::wrapper! {
    pub struct PixelDetectiveWindow(ObjectSubclass<imp::PixelDetectiveWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl PixelDetectiveWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)])
            .expect("Failed to create PixelDetectiveWindow")
    }

    fn open_file(&self) {
        let app = gio::Application::default()
            .expect("Failed to retrieve application singleton")
            .downcast::<gtk::Application>()
            .unwrap();
        let window = app.active_window()
            .unwrap()
            .downcast::<gtk::Window>()
            .unwrap();
        let chooser = gtk::FileChooserNative::builder()
            .modal(true)
            .title("Open File")
            .action(gtk::FileChooserAction::Open)
            .transient_for(&window)
            .build();

        chooser.connect_response(
            clone!(@strong chooser, @weak self as app => move |_, response| {
                if response == gtk::ResponseType::Accept {
                    app.process_files(&chooser.files());
                }
            }),
        );

        chooser.show();
    }

    fn process_files(&self, files: &gio::ListModel) {
        for pos in 0..files.n_items() {
            let gfile = files.item(pos).unwrap().downcast::<gio::File>().unwrap();
            let filename = gfile.uri();
            let path = Path::new(&filename[7..]);
            let mut file = match File::open(&path) {
                Ok(f) => f,
                Err(e) => panic!("Could not open {}: {}", path.display(), e),
            };

            let mut image_data = Vec::new();
            file.read_to_end(&mut image_data).unwrap();
            self.imp().canvas.set_draw_func(move |canvas, context, width, height| {
                for (i, triple) in image_data.chunks_exact(3).enumerate() {
                    let r = triple[0] as f64 * 0.00390625;
                    let g = triple[1] as f64 * 0.00390625;
                    let b = triple[2] as f64 * 0.00390625;
                    let x = i as i32 % width;
                    let y = i as i32 / width;

                    if y < height {
                        context.set_source_rgb(r, g, b);
                        context.rectangle(x as f64, y as f64, 1.0, 1.0);
                        context.fill().unwrap();
                    }
                }
            });
        }
    }
}

